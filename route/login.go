package route

import (
	"github.com/labstack/echo/v4"
	"project-end-server/controller"
)

func RouteMember(e *echo.Echo) {
	control := controller.NewControllerLogin()
	e.POST("/login",control.Login)
	e.POST("/logout",control.Logout)
}