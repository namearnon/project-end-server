package route

import (
	"github.com/labstack/echo/v4"
	"project-end-server/controller"
)

func RouteModerator(e *echo.Echo) {
	control := controller.NewModeratorController()
	e.POST("/getboard",control.GetBoard)
	e.POST("/createboard",control.CreateBoard)
	e.POST("/deleteboard",control.DeleteBoard)
	e.POST("/getevalution",control.GetEvalution)
	e.POST("/setevalution",control.SetEvalution)
	e.POST("/getdatamoderator",control.GetDataModerator)
	e.POST("/getdatateamleader",control.GetDataTeamleader)
	e.POST("/getdatamember",control.GetDataMember)
	e.POST("/leaveboard",control.LeaveBoard)
	e.POST("/inboard",control.Inboard)
	e.POST("/exportcsv", control.ExportCSV)



}
