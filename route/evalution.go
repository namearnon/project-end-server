package route

import (
	"github.com/labstack/echo/v4"
	"project-end-server/controller"
)

func RouteEvalution(e *echo.Echo) {
	control := controller.NewEvalutionController()
	e.POST("/getform",control.GetFormQrcode)
	e.POST("/addpoint",control.AddPoint)
}
