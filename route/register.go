package route

import (
	"github.com/labstack/echo/v4"
	"project-end-server/controller"
)

func RouteRegister(e *echo.Echo) {
	control := controller.NewControllerRegister()
	e.POST("/register",control.Register)
	e.POST("/signingoogle",control.SigninGoogle)

}
