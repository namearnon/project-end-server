package route

import (
	"github.com/labstack/echo/v4"
	"project-end-server/controller"
)

func RouteAdmin(e *echo.Echo) {
	control := controller.NewControllerAdmin()
	e.POST("/addmoderator",control.AddModerator)
	e.POST("/loginadmin",control.LoginAdmin)
	e.POST("/getmoderator",control.GetMorator)
	e.POST("/deletemember",control.DeleteMember)
	e.POST("/getmember",control.GetMember)
	e.POST("/deletemoderator",control.DeleteModerator)

}
