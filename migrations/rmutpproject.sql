CREATE
    DATABASE IF NOT EXISTS `rmutpproject` CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `rmutpproject`;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE TABLE `admin`
(
    `id`        varchar(32) COLLATE utf8_unicode_ci NOT NULL PRIMARY KEY,
    `name`      varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `username`  varchar(10) COLLATE utf8_unicode_ci NOT NULL,
    `password`  varchar(200) COLLATE utf8_unicode_ci NOT NULL,
    `status_id` int(1) COLLATE utf8_unicode_ci  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


INSERT INTO `admin` (`id`,`name`, `username`, `password`, `status_id`)
VALUES ('admin', 'admin','admin', '0759503007595030', '0');
-- --------------------------------------------------------

CREATE TABLE `board`
(
    `id`        varchar(32) COLLATE utf8_unicode_ci  NOT NULL PRIMARY KEY,
    `create_by` varchar(32) COLLATE utf8_unicode_ci  NOT NULL,
    `title`     varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `des`       varchar(100) COLLATE utf8_unicode_ci NOT NULL

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `board_question`
(
    `id`       int(11)                              NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `board_id` varchar(32) COLLATE utf8_unicode_ci  NOT NULL,
    `form`     int(1) COLLATE utf8_unicode_ci NOT NULL,
    `question` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `section` int(2) COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;


CREATE TABLE `member`
(
    `id`       varchar(32) COLLATE utf8_unicode_ci NOT NULL PRIMARY KEY,
    `email`    varchar(50) COLLATE utf8_unicode_ci NOT NULL,
    `name`     varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
    `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
    `access_id`   int (1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `member_board`
(
    `id`        int(11)                             NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `board_id`  varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `member_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `status_id` int (1) COLLATE utf8_unicode_ci  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `status`
(
    `id` varchar(1) COLLATE utf8_unicode_ci  NOT NULL PRIMARY KEY,
    `status`    varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO `status` (`id`, `status`)
VALUES ('0', 'admin'),
       ('1', 'moderator'),
       ('2', 'manager'),
       ('3', 'member');

CREATE TABLE `result`
(
    `id`       int(11)                              NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `board_id` varchar(32) COLLATE utf8_unicode_ci  NOT NULL,
    `member_id` varchar(32) COLLATE utf8_unicode_ci  NOT NULL,
    `question_section` int(2) COLLATE utf8_unicode_ci NOT NULL,
    `point`    int(1)                               NOT NULL,
    `count`    int(10)   NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `login`
(
    `id`    varchar(32) COLLATE utf8_unicode_ci   NOT NULL PRIMARY KEY,
    `token` varchar(1400) COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `board_answer`
(
    `id`       int(11)                              NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `board_id` varchar(32) COLLATE utf8_unicode_ci  NOT NULL,
    `answer` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `question_section` int(2) COLLATE utf8_unicode_ci NOT NULL

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

  CREATE TABLE `access`
(
    `id` int(1) COLLATE utf8_unicode_ci  NOT NULL PRIMARY KEY,
    `access`    varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

INSERT INTO `access` (`id`, `access`)
VALUES (0, 'normal'),
       (1, 'gmail');

CREATE TABLE `teamleader`
(
    `id`      int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `teamleader_id`    varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `member_id`     varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

  CREATE TABLE `checktime`
(
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `time_id`    varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `board_id`    varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `created_at` datetime NOT NULL

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
