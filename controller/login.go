package controller

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"log"
	"net/http"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service/auth"
)
type ControllerLogin struct {

}

func NewControllerLogin() *ControllerLogin{
	return &ControllerLogin{}
}
func (*ControllerLogin)Login(c echo.Context) error {
	cc := c.(core.IContext)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	result := auth.NewLoginService(cc).Login(&postData)
	return c.JSON(http.StatusAccepted,result)
}

func (*ControllerLogin)Logout(c echo.Context) error {
	cc := c.(core.IContext)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	result := auth.NewLoginService(cc).Logout(&postData)
	return c.JSON(http.StatusAccepted,result)
}
