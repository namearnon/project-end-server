package controller

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"log"
	"net/http"
	"project-end-server/config"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service/moderator"
)

type ModeratorController struct {

}

func NewModeratorController() *ModeratorController{
	return &ModeratorController{}
}

func (*ModeratorController)CreateBoard(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewModeratorService(cc).CreateBoard(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)GetBoard(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewModeratorService(cc).GetBoard(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)DeleteBoard(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewModeratorService(cc).DeleteBoard(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)SetEvalution(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewModeratorService(cc).SetEvalution(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)GetEvalution(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewModeratorService(cc).GetEvalution(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)GetDataModerator(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewModeratorService(cc).GetDataModerator(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)GetDataTeamleader(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewTeamleaderService(cc).GetDataTeamleader(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)GetDataMember(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewMemberService(cc).GetDataMember(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)LeaveBoard(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewMemberService(cc).LeaveBoard(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)Inboard(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewMemberService(cc).Inboard(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*ModeratorController)ExportCSV(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := moderator.NewModeratorService(cc).ExportCsv(&postData)
	return c.JSON(http.StatusOK,result)
}


