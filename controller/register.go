package controller

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"log"
	"net/http"
	"project-end-server/config"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service/auth"
)

type ControllerRegister struct {

}

func NewControllerRegister() *ControllerRegister{
	return &ControllerRegister{}
}
func (*ControllerRegister)Register(c echo.Context) error {
	cc := c.(core.IContext)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	result := auth.NewServiceRegister(cc).Register(&postData)
	return c.JSON(http.StatusAccepted,result)
}

func (*ControllerRegister)SigninGoogle(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get(config.XTOKEN)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	result := auth.NewServiceRegister(cc).GoogleSignUp(&postData,token)
	return c.JSON(http.StatusAccepted,result)
}

