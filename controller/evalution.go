package controller

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"log"
	"net/http"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service/evalution"
)

type EvalutionController struct {

}

func NewEvalutionController() *EvalutionController{
	return &EvalutionController{}
}

func (*EvalutionController)GetFormQrcode(c echo.Context) error {
	cc := c.(core.IContext)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	result := evalution.NewEvalutionService(cc).GetFormQrcode(&postData)
	return c.JSON(http.StatusOK,result)
}

func (*EvalutionController)AddPoint(c echo.Context) error {
	cc := c.(core.IContext)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	result := evalution.NewEvalutionService(cc).AddPoint(&postData)
	return c.JSON(http.StatusOK,result)
}