package controller

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"log"
	"net/http"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service/admin"
)

type ControllerAdmin struct {

}

func NewControllerAdmin() *ControllerAdmin {
	return &ControllerAdmin{}
}

func (*ControllerAdmin)AddModerator(c echo.Context) error{
	cc := c.(core.IContext)
	token := c.Request().Header.Get("x-token")
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := admin.NewServiceAdmin(cc).AddModerator(&postData)
	return c.JSON(http.StatusCreated,result)
}

func (*ControllerAdmin)LoginAdmin(c echo.Context) error {
	cc := c.(core.IContext)
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	result := admin.NewServiceAdmin(cc).LoginAdmin(&postData)
	return c.JSON(http.StatusAccepted,result)
}

func (*ControllerAdmin)GetMorator(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get("x-token")
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := admin.NewServiceAdmin(cc).GetModerator(&postData)
	return c.JSON(http.StatusAccepted,result)
}

func (*ControllerAdmin)GetMember(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get("x-token")
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := admin.NewServiceAdmin(cc).GetMember(&postData)
	return c.JSON(http.StatusAccepted,result)
}

func (*ControllerAdmin)DeleteModerator(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get("x-token")
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := admin.NewServiceAdmin(cc).DeleteModerator(&postData)
	return c.JSON(http.StatusAccepted,result)
}

func (*ControllerAdmin)DeleteMember(c echo.Context) error {
	cc := c.(core.IContext)
	token := c.Request().Header.Get("x-token")
	body , err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Println(err)
	}
	var postData model.Data
	json.Unmarshal(body,&postData)
	check := model.CheckToken(cc,token,postData.ID)
	if !check {
		return c.JSON(http.StatusUnauthorized, map[string]interface{}{
			"message" : "Unauthorized",
		})
	}
	result := admin.NewServiceAdmin(cc).DeleteMember(&postData)
	return c.JSON(http.StatusAccepted,result)
}