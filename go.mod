module project-end-server

go 1.12

require (
	github.com/boombuler/barcode v1.0.0
	github.com/emersion/go-sasl v0.0.0-20190704090222-36b50694675c
	github.com/emersion/go-smtp v0.12.0
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/echo/v4 v4.1.10
	github.com/minio/minio-go/v6 v6.0.49
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pilu/config v0.0.0-20131214182432-3eb99e6c0b9a // indirect
	github.com/pilu/fresh v0.0.0-20190826141211-0fa698148017 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/text v0.3.2
)
