package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"log"
	"project-end-server/middlewares"
	"project-end-server/route"
)

func main() {
	e := echo.New()
	e.Use(middlewares.Core)
	e.Use(middleware.Static(""))
	e.Use(middleware.Static("./images"))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: true,
		AllowMethods:     []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
		AllowOrigins:     []string{"*"},
	}))
	route.RouteAdmin(e)
	route.RouteRegister(e)
	route.RouteMember(e)
	route.RouteModerator(e)
	route.RouteEvalution(e)
	log.Fatal(e.Start(middlewares.GetPort()))
}
