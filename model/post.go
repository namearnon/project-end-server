package model

type Data struct {
	ID string `json:"id"`
	BoardID string `json:"boardid"`
	TimeID string `json:"timeid"`
	TeamLeaderID string `json:"teamleaderid"`
	ModeratorID string `json:"moderatorid"`
	Status string `json:"status"`
	Name string `json:"name"`
	Title string `json:"title"`
	Username string `json:"username"`
	Password string `json:"password"`
	Des string `json:"des"`
	Items []Evalution `json:"items"`
	Point []Point `json:"point"`
}

type Evalution struct {
	Question string `json:"questions"`
	Selected string `json:"selected"`
	Additionals []Additionals `json:"additionals"`
}

type Additionals struct {
	Answer string `json:"answer"`
}

type Point struct {
	Question string `json:"questions"`
	Options []Options `json:"options"`
	Checked int `json:"checked"`
	Status string `json:"status"`
}

type Options struct {
	Maxrate string `json:"maxrate,omitempty"`
	Txt string `json:"txt"`
	Val string `json:"val"`
}

