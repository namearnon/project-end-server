package model

import (
	"encoding/base64"
	"log"
	"math/rand"
	"project-end-server/core"
)

type Login struct {
	ID    string
	Token string
}

type Member struct {
	ID string
	Email string
	Name string
	AccessID int
	Password string
}

type MemberJoinMemberBoard struct {
	ID string
	Email string
	Name string
	AccessID int
	Password string
	BoardID string
	MemberID string
	TeamleaderID string
	StatusID int
}

func GenerateToken(n int) string {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}

	return base64.URLEncoding.EncodeToString(b)
}

func CheckToken(c core.IContext , token , id string) bool {
	db , err := c.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	check := Login{}
	checktoken := db.Table("login").Where("token = ? AND id = ?",token,id).Scan(&check).NewRecord(check)
	if checktoken {
		return false
	}
	return true
}