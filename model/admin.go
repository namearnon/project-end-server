package model

import (
	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"github.com/minio/minio-go/v6"
	"image/png"
	"log"
	"os"
	"project-end-server/core"
)

type Admin struct {
	ID       string
	Name     string
	Username string
	Password string
	StatusID int
}

func GenQrcode( bucketName , objectName , filePath, contentType , url string ,ctx core.IContext) {
	qrCode, err := qr.Encode(url, qr.M, qr.Auto)
	if err != nil {
		log.Println(err)
	}
	qrCode, err = barcode.Scale(qrCode, 200, 200)
	if err != nil {
		log.Println(err)
	}
	file, err := os.Create(filePath)
	if err != nil {
		log.Fatalln(err)
	}
	minios , err := ctx.Minio()
	if err != nil {
		log.Println(err)
	}
	log.Println(minios)
	png.Encode(file, qrCode)
	n , err := minios.FPutObject(bucketName, objectName[1:], filePath, minio.PutObjectOptions{ContentType: contentType})
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("Successfully uploaded %s of size %d\n", objectName, n)
}
