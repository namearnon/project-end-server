package model

import (
	"golang.org/x/crypto/bcrypt"
	"log"
)

func HashPassword(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
	}
	return string(hash)
}

func VerifyPassword(hashedPwd string, Pwd []byte) bool {
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, Pwd)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
