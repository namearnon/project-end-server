package model

import "time"

type Board struct {
	ID string
	CreateBy string
	Title string
	Des string
}

type BoardJoinBoardQuestion struct {
	ID string
	CreateBy string
	Title string
	Form int
	Des string
	BoardID string
	Question string
	Section int
}

type ResultJoinMember struct {
	ID string
	BoardID string
	MemberID string
	TeamleaderID string
	Question string
	Point int
	Count int
	Email string
	Name string
	AccessID int
	Password string
}


type Result struct {
	ID string
	BoardID string
	MemberID string
	QuestionSection int
	Point int
	Count int
}

type ResultJoinTeamleader struct {
	ID string
	BoardID string
	MemberID string
	QuestionSection int
	Point int
	Count int
	TeamleaderID string
}

type BoardQuestion struct {
	ID string
	BoardID string
	Form int
	Question string
	Section int
}

type BoardAnswer struct {
	ID string
	BoardID string
	Answer string
	QuestionSection int
}

type Checktime struct {
	ID int
	TimeID string
	BoardID string
	CreatedAt time.Time

}

func InTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}