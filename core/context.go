package core

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"github.com/minio/minio-go/v6"
)

type IContext interface {
	echo.Context
	DB() (*gorm.DB, error)
	PingDB() error
	Minio() (*minio.Client,error)
}

type Context struct {
	echo.Context
}

func (c *Context) DB() (*gorm.DB, error) {
	db, err := NewDatabase().Connect()
	if err != nil {
		return nil, err
	}
	db.LogMode(true)
	return db, nil
}

func (c *Context) PingDB() error {
	err := NewDatabase().PingDB()
	if err != nil {
		return err
	}
	return nil
}

func (c *Context) Minio() (*minio.Client,error){
	minio,err := NewMinio().ConnectMinio()
	if err != nil {
		return nil,err
	}
	return minio,nil
}