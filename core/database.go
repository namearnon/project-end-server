package core

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Database struct {
	Name     string
	Host     string
	User     string
	Password string
	Port     string
	Location string
}

func NewDatabase() *Database {
	return &Database{
		Name: "heroku_44a9f903e1696e0",
		//Host:     "mariadb",
		Host:     "us-cdbr-iron-east-01.cleardb.net",
		User:     "b81e733dd24cbb",
		Password: "3a8f6f48",
		Port:     "3306",
		Location : "Asia%2fBangkok",
	}
}

//func NewDatabase() *Database {
//	return &Database{
//		Name: "rmutpproject",
//		//Host:     "mariadb",
//		Host:     "localhost",
//		User:     "root",
//		Password: "123456",
//		Port:     "3306",
//		Location : "Asia%2fBangkok",
//	}
//}

// ConnectDB to connect database
func (db *Database) Connect() (*gorm.DB, error) {
	newDB, err := gorm.Open("mysql",
		fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8&parseTime=True&loc=Local&multiStatements=True&loc=%v",
			db.User, db.Password, db.Host, db.Port, db.Name,db.Location,
		))
	newDB.SingularTable(true)
	if err != nil {
		return nil, err
	}
	err = newDB.DB().Ping()
	if err != nil {
		return nil , err
	}
	newDB.DB().SetMaxIdleConns(50)
	newDB.DB().SetMaxOpenConns(500)
	newDB.LogMode(true)
	return newDB, nil
}

func (db *Database) PingDB() error {
	_, err := db.Connect()
	return err
}
