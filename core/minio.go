package core

import (
	"github.com/minio/minio-go/v6"
	"log"
)

type Minio struct {
	Endpoint string
	KeyID string
	Secret string
	SSL bool
	BucketName string
	Location string
}
func NewMinio() *Minio{
	return &Minio{
		Endpoint: "s3.amazonaws.com",
		KeyID:    "AKIAJL7ZKGLMBYUXEOBA",
		Secret:   "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",
		SSL:      true,
		BucketName : "actingprojectkingkong",
		Location : "us-east-1",
	}
}

//func NewMinio() *Minio{
//	return &Minio{
//		Endpoint: "localhost:9000",
//		KeyID:    "PROJECTEND",
//		Secret:   "123456789",
//		SSL:      false,
//		BucketName : "actingprojectkingkong",
//		Location : "us-east-1",
//	}
//}

func (s *Minio)ConnectMinio() (*minio.Client,error){
	// Initialize minio client object.
	minioClient, err := minio.New(
		s.Endpoint, s.KeyID, s.Secret, s.SSL)
	err = minioClient.MakeBucket(s.BucketName, s.Location)
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, err := minioClient.BucketExists(s.BucketName)
		if err == nil && exists {
			log.Printf("We already own %s\n", s.BucketName)
		}
	} else {
		log.Printf("Successfully created %s\n", s.BucketName)
	}
	return minioClient , nil
}