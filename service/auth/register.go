package auth

import (
	"log"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service"
)

type ServiceRegister struct {
	ctx core.IContext
}

func NewServiceRegister(ctx core.IContext) service.InterfaceRegister {
	return &ServiceRegister{
		ctx: ctx,
	}
}

func (s *ServiceRegister) Register(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	member := model.Member{}
	checkmember := db.Table("member").Where("email = ?", data.Username).Scan(&member).NewRecord(member)
	if !checkmember {
		return map[string]interface{}{
			"message": "Email exist",
		}
	}
	id, err := model.NewUUID()
	if err != nil {
		log.Println(err)
	}
	db.Create(&model.Member{
		ID:       id,
		Email:    data.Username,
		Name:     data.Name,
		AccessID: 0,
		Password: model.HashPassword([]byte(data.Password)),
	})
	return map[string]interface{}{
		"message": "success",
	}
}

func (s *ServiceRegister) GoogleSignUp(data *model.Data , tokens string) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	id, err := model.NewUUID()
	if err != nil {
		log.Println(err)
	}
	member := model.Member{}
	board := model.MemberBoardJoinTeamleader{}
	token := model.Login{}
	checkmember := db.Table("member").Where("email = ?", data.Username).Scan(&member).NewRecord(member)
	db.Table("member_board as mb").Select("mb.board_id,mb.status_id,t.teamleader_id").Where("member_id = ?", member.ID).Joins("left join teamleader as t on t.member_id = mb.member_id").Scan(&board)
	if !checkmember {
		checktoken := db.Table("login").Where("id = ?", member.ID).Scan(&token).NewRecord(token)
		if !checktoken {
			db.Delete(&token)
		}
	} else {
		db.Create(model.Member{
			ID:       id,
			Email:    data.Username,
			Name:     data.Name,
			AccessID: 1,
			Password: "gmail",
		})
	}
	db.Create(&model.Login{
		ID:    member.ID,
		Token: tokens,
	})

	return map[string]interface{}{
		"id":       member.ID,
		"name":     member.Name,
		"status":   board.StatusID,
		"teamleaderid" : board.TeamleaderID,
		"token":    tokens,
		"username": data.Username,
		"boardid" : board.BoardID,
	}
}
