package auth

import (
	"log"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service"
)

type LoginService struct {
	ctx core.IContext
}

func NewLoginService(ctx core.IContext) service.InterfaceLogin {
	return &LoginService{
		ctx: ctx,
	}
}

func (s *LoginService) Login(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	moderator := model.Admin{}
	login := model.Login{}
	checkmode := db.Table("admin").Where("username = ? AND status_id = '1'", data.Username).Scan(&moderator).NewRecord(moderator)
	if !checkmode {
		checktoken := db.Table("login").Where("id = ?", moderator.ID).Scan(&login).NewRecord(login)
		if !checktoken {
			db.Delete(&login)
		}
		if !model.VerifyPassword(moderator.Password, []byte(data.Password)) {
			return map[string]interface{}{
				"message": "Password not match",
			}
		}
		token := model.GenerateToken(150)
		db.Create(&model.Login{
			ID:    moderator.ID,
			Token: token,
		})
		return map[string]interface{}{
			"id":       moderator.ID,
			"username": moderator.Username,
			"name":     moderator.Name,
			"token":    token,
			"status":   moderator.StatusID,
		}
	}
	member := model.Member{}
	memberboard := model.MemberBoardJoinTeamleader{}
	checkmember := db.Table("member").Where("email = ?", data.Username).Scan(&member).NewRecord(member)
	db.Table("member_board as mb").Select("mb.board_id,mb.status_id,t.teamleader_id").Where("mb.member_id = ?", member.ID).Joins("left join teamleader as t on t.member_id = mb.member_id").Scan(&memberboard)
	if !checkmember {
		if member.AccessID == 1 {
			return map[string]interface{}{
				"message" : "this email login with google",
			}
		}
		checktoken := db.Table("login").Where("id = ?", member.ID).Scan(&login).NewRecord(login)
		if !checktoken {
			db.Delete(&login)
		}
		if !model.VerifyPassword(member.Password, []byte(data.Password)) {
			return map[string]interface{}{
				"message": "Password not match",
			}
		}
		token := model.GenerateToken(150)
		db.Create(&model.Login{
			ID:    member.ID,
			Token: token,
		})
		return map[string]interface{}{
			"id":           member.ID,
			"teamleaderid": memberboard.TeamleaderID,
			"username":     member.Email,
			"name":         member.Name,
			"token":        token,
			"status":       memberboard.StatusID,
			"boardid":      memberboard.BoardID,
		}
	}
	return map[string]interface{}{
		"message": "Not have id in system",
	}

}

func (s *LoginService) Logout(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	db.Delete(&model.Login{}, "id = ?", data.ID)
	return map[string]interface{}{
		"message": "success",
	}
}
