package moderator

import (
	"fmt"
	"log"
	"net/url"
	"project-end-server/config"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service"
	"time"
)

type MemberService struct {
	ctx core.IContext
}

func NewMemberService(ctx core.IContext) service.InterfaceMember {
	return &MemberService{
		ctx: ctx,
	}
}

func (s *MemberService) Inboard(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	if data.BoardID != "" && data.TeamLeaderID == "" {
		if data.Status != "2"{
			return map[string]interface{}{
				"message": "Status Error",
			}
		}
	}
	if data.BoardID != "" && data.TeamLeaderID != "" {
		if data.Status != "3"{
			return map[string]interface{}{
				"message": "Status Error",
			}
		}
	}
	defer db.Close()
	admin := model.Admin{}
	board := model.Board{}
	var memberboard, checkmember model.MemberBoard
	var member model.MemberBoard
	boardcheck := db.Table("board").Where("id = ?",data.BoardID).Scan(&board).NewRecord(board)
	if boardcheck {
		return map[string]interface{}{
			"message": "Not have board in system",
		}
	}
	check := db.Table("admin").Where("id = ?", data.ID).Scan(&admin).NewRecord(admin)
	if !check {
		return map[string]interface{}{
			"message": "You is moderator",
		}
	}
	db.Table("member_board").Where("board_id = ? AND member_id = ?", data.BoardID, data.ID).Scan(&memberboard)
	if memberboard.StatusID == "" {
		db.Table("member_board").Where("member_id = ?", data.ID).Scan(&checkmember)
		if checkmember.MemberID != "" {
			return map[string]interface{}{
				"message": "You have already in board",
			}
		}
		if data.Status == "3" {
			db.Table("member_board").Where("member_id = ? AND status_id = '2'",data.TeamLeaderID).Scan(&member)
			if member.ID == "" {
				return map[string]interface{}{
					"message": "this teamleader not exist ",
				}
			}
		}
		memberboard = model.MemberBoard{
			BoardID:      data.BoardID,
			MemberID:     data.ID,
			StatusID:     data.Status,
		}
		db.Create(&memberboard)
		if data.Status == "3" {
			db.Create(&model.Teamleader{
				TeamleaderID: data.TeamLeaderID,
				MemberID:     data.ID,
			})
		}
		if data.Status == "3" {
			model.GenQrcode(config.Bucket,"/evalution/"+data.BoardID+"/"+data.ID+".png","./images/qrcode/evalution/"+data.ID+".png","application/octet-stream",  "https://" + config.URL + "/form?boardid=" + data.BoardID + "&id=" + data.ID + "&teamleaderid=" + data.TeamLeaderID,s.ctx)
		}
		if data.Status == "2" {
			model.GenQrcode(config.Bucket,"/teamleader/"+data.BoardID+"/"+data.ID+".png","./images/qrcode/teamleader/"+data.ID+".png","application/octet-stream", "https://" + config.URL + "/checkstatus?boardid=" + data.BoardID + "&status=3&teamleaderid=" + data.ID,s.ctx)
		}
	} else {
		if memberboard.StatusID == data.Status {
			return map[string]interface{}{
				"message": "You in this board",
			}
		}
		if memberboard.StatusID == "2" {
			return map[string]interface{}{
				"message": "You is Teamleader",
			}
		} else if memberboard.StatusID == "3" {
			return map[string]interface{}{
				"message": "You is Member",
			}
		}
	}
	return map[string]interface{}{
		"message": "success",
	}
}

func (s *MemberService) GetDataMember(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	result := []model.Result{}
	var n int
	db.Table("result").Where("board_id = ? AND member_id = ?",data.BoardID,data.ID).Scan(&result)
	if len(result) != 0 {
		n = result[len(result) - 1].Count
	}
	minioClient, err := s.ctx.Minio()
	reqParams := make(url.Values)
	reqParams.Set("response-content-disposition", "attachment; filename=\""+data.ID+".png\"")

	// Generates a presigned url which expires in a day.
	presignedURL, err := minioClient.PresignedGetObject(config.Bucket, "evalution/"+data.BoardID+"/"+data.ID+".png", time.Second*24*60*60, reqParams)
	if err != nil {
		fmt.Println(err)
	}
	return map[string]interface{}{
		"qrcode": presignedURL.String(),
		"link":   "https://" + config.URL + "/form?boardid=" + data.BoardID + "&id=" + data.ID + "&teamleaderid=" + data.TeamLeaderID,
		"n" : n,
		"status": "3",
	}
}

// ทำ Restful Api ด้วย
func (s *MemberService) LeaveBoard(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	db.Delete(&model.MemberBoard{}, "board_id = ? AND member_id = ? OR board_id = ? AND teamleader_id = ?", data.BoardID, data.ID, data.BoardID, data.ID)
	return map[string]interface{}{
		"message": "success",
	}
}
