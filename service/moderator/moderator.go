package moderator

import (
	"encoding/csv"
	"fmt"
	"log"
	"math"
	"net/url"
	"os"
	"project-end-server/config"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service"
	"strconv"
	"time"
)

type ModeratorService struct {
	ctx core.IContext
}

func NewModeratorService(ctx core.IContext) service.InterfaceModerator {
	return &ModeratorService{
		ctx: ctx,
	}
}

func (s *ModeratorService) GetBoard(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	allboard := []map[string]interface{}{}
	board := []model.Board{}
	db.Table("board").Where("create_by = ?", data.ID).Scan(&board)
	for _, v := range board {
		allboard = append(allboard, map[string]interface{}{
			"name":    v.Title,
			"boardid": v.ID,
		})
	}
	return map[string]interface{}{
		"message": allboard,
	}
}

func (s *ModeratorService) SetEvalution(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	db.Delete(model.BoardQuestion{}, "board_id = ?", data.BoardID)
	db.Delete(model.BoardAnswer{}, "board_id = ?", data.BoardID)
	for i, v := range data.Items {
		for _, value := range v.Additionals {
			db.Create(&model.BoardAnswer{
				BoardID:         data.BoardID,
				Answer:          value.Answer,
				QuestionSection: i + 1,
			})
		}
		form, _ := strconv.Atoi(v.Selected)
		db.Create(&model.BoardQuestion{
			BoardID:  data.BoardID,
			Form:     form,
			Question: v.Question,
			Section:  i + 1,
		})
	}
	db.Table("board").Where("id = ?", data.BoardID).Updates(map[string]interface{}{
		"title": data.Title,
		"des":   data.Des,
	})
	return map[string]interface{}{
		"message": "success",
	}
}

func (s *ModeratorService) GetDataModerator(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()

	result := []model.ResultJoinTeamleader{}
	question := []model.BoardQuestion{}
	member := []model.MemberJoinMemberBoard{}
	checkTime := []model.Checktime{}
	var mean, sd, sd2, na float64
	var point, n, num int
	var pointData, count []int
	var pointquestion, averageTeam []map[string]interface{}
	db.Table("member_board as mb").Select("mb.member_id,m.name").Where("mb.board_id = ? AND mb.status_id = '2'", data.BoardID).Joins("left join member as m on m.id = mb.member_id").Scan(&member)
	db.Table("board_question").Where("board_id = ?", data.BoardID).Scan(&question)
	db.Table("result as r").Select("r.question_section,r.point,t.teamleader_id,r.count,r.member_id").Where("board_id = ?", data.BoardID).Joins("left join teamleader as t on t.member_id = r.member_id").Scan(&result)
	db.Table("checktime").Select("DISTINCT(time_id)").Where("board_id = ?", data.BoardID).Scan(&checkTime)
	for _, vs := range question {
		for _, value := range []int{1, 2, 3, 4, 5} {
			for _, v := range result {
				if vs.Section == v.QuestionSection {
					if value == v.Point {
						n++
					}
				}
			}
			pointData = append(pointData, n)
			n = 0
		}
		pointquestion = append(pointquestion, map[string]interface{}{
			"xaxis": map[string]interface{}{
				"categories": []string{"น้อยที่สุด", "น้อย", "ปานกลาง", "มาก", "มากที่สุด"},
			},
			"series": []map[string]interface{}{
				{
					"name": vs.Question,
					"data": pointData,
				},
			},
		})
		pointData = nil
	}
	n = 0
	//****************************************************************************************************************//
	for i := 1; i <= 5; i++ {
		for _, value := range result {
			if value.Point == i {
				n++
			}
		}
		count = append(count, n)
		n = 0
	}
	n = 0
	//****************************************************************************************************************//
	for _, v := range member {
		for _, value := range result {
			if v.MemberID == value.TeamleaderID {
				point += value.Point
				num += 1
				if n < value.Count {
					n = value.Count
				} else if n > value.Count {
					na += float64(n)
					n = 0
				}
			}
		}
		na += float64(n)
		mean = float64(point) / float64(num)
		if point != 0 {
			for _, value := range result {
				if v.MemberID == value.TeamleaderID {
					sd += math.Pow(float64(value.Point)-mean, 2)
				}
			}
			sd = math.Sqrt(sd / float64(num))
			sd2 = math.Pow(sd, 2)
		}
		averageTeam = append(averageTeam, map[string]interface{}{
			"xaxis": map[string]interface{}{
				"categories": []string{"ค่าเฉลี่ย", "ค่าส่วนเบี่ยงเบน", "ค่าความแปรปรวน", "จำนวนครั้งที่ได้คะแนน", "คะแนนทั้งหมด"},
			},
			"series": []map[string]interface{}{
				{
					"name": v.Name,
					"data": []string{fmt.Sprintf("%.2f", mean), fmt.Sprintf("%.2f", sd), fmt.Sprintf("%.2f", sd2), fmt.Sprintf("%.2f", na), fmt.Sprintf("%d", point)},
				},
			},
		})
		mean, sd, sd2, n, point, na, num = 0, 0, 0, 0, 0, 0, 0
	}
	mean, sd, sd2, n, point, na, num = 0, 0, 0, 0, 0, 0, 0
	//****************************************************************************************************************//
	for _, value := range member {
		for _, v := range result {
			if v.TeamleaderID == value.MemberID {
				point += v.Point
				if n < v.Count {
					n = v.Count
				}
			}
		}
		na += float64(n)
		n = 0
	}
	na += float64(n)
	if point != 0 {
		mean = float64(point) / float64(len(result))
		for _, v := range result {
			sd += math.Pow(float64(v.Point)-mean, 2)
		}
		sd = math.Sqrt(sd / float64(len(result)))
		sd2 = math.Pow(sd, 2)
	}
	if averageTeam == nil {
		averageTeam = []map[string]interface{}{
			{
				"name": "serie-1",
				"data": []float64{0, 0, 0, 0, 0},
			},
		}
	}
	minioClient, err := s.ctx.Minio()
	reqParams := make(url.Values)
	reqParams.Set("response-content-disposition", "attachment; filename=\""+data.BoardID+".png\"")

	// Generates a presigned url which expires in a day.
	presignedURL, err := minioClient.PresignedGetObject(config.Bucket, "moderator/"+data.BoardID+".png", time.Second*24*60*60, reqParams)
	if err != nil {
		fmt.Println(err)
	}

	if pointquestion == nil {
		pointquestion = append(pointquestion, map[string]interface{}{
			"xaxis": map[string]interface{}{
				"categories": []string{"น้อยที่สุด", "น้อย", "ปานกลาง", "มาก", "มากที่สุด"},
			},
			"series": []map[string]interface{}{
				{
					"name": "ไม่มี",
					"data": []int{0, 0, 0, 0, 0},
				},
			},
		})
	}

	return map[string]interface{}{
		"evalution": map[string]interface{}{
			"mean": fmt.Sprintf("%.2f", mean),
			"sd":   fmt.Sprintf("%.2f", sd),
			"sd2":  fmt.Sprintf("%.2f", sd2),
			"many": fmt.Sprintf("%d", len(checkTime)),
			"n":    fmt.Sprintf("%.2F", na),
		},
		"chartquestion": pointquestion,
		"piechart": map[string]interface{}{
			"series": count,
			"label":  []string{"น้อยที่สุด", "น้อย", "ปานกลาง", "มาก", "มากที่สุด"},
		},
		"linechart":        averageTeam,
		"qrcodeTeamleader": presignedURL.String(),
		"link":             "https://" + config.URL + "/checkstatus?boardid=" + data.BoardID + "&status=2",
	}
}

func (s *ModeratorService) GetEvalution(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	question := []model.BoardJoinBoardQuestion{}
	answer := []model.BoardAnswer{}
	allquestion := []map[string]interface{}{}
	allanswer := []map[string]interface{}{}
	db.Table("board_question as bq").Select("bq.question,bq.form,b.title,b.des").Where("bq.board_id = ?", data.BoardID).Joins("left join board as b on b.id = bq.board_id").Scan(&question)
	db.Table("board_answer").Where("board_id = ?", data.BoardID).Scan(&answer)

	for i, v := range question {
		for _, value := range answer {
			if value.QuestionSection == i+1 {
				allanswer = append(allanswer, map[string]interface{}{
					"answer": value.Answer,
				})
			}
		}
		allquestion = append(allquestion, map[string]interface{}{
			"selected":    v.Form,
			"questions":   v.Question,
			"additionals": allanswer,
		})
		allanswer = nil
	}
	return map[string]interface{}{
		"title": question[0].Title,
		"des":   question[0].Des,
		"items": allquestion,
	}
}

func (s *ModeratorService) CreateBoard(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	id, err := model.NewUUID()
	if err != nil {
		log.Println(err)
	}

	db.Create(&model.Board{
		ID:       id,
		CreateBy: data.ID,
		Title:    data.Title,
		Des:      "Please Set Desciption",
	})
	db.Create(&model.BoardQuestion{
		BoardID:  id,
		Form:     0,
		Question: "Question",
		Section:  1,
	})
	for _, v := range []string{"น้อยที่สุด", "น้อย", "ปานกลาง", "มาก", "มากที่สุด"} {
		db.Create(&model.BoardAnswer{
			BoardID:         id,
			Answer:          v,
			QuestionSection: 1,
		})
	}
	db.Close()
	model.GenQrcode(config.Bucket, "/moderator/"+id+".png", "./images/qrcode/moderator/"+id+".png", "application/octet-stream", "https://"+config.URL+"/checkstatus?boardid="+id+"&status=2", s.ctx)
	return map[string]interface{}{
		"boardid": id,
		"name":    data.Title,
	}
}

func (s *ModeratorService) DeleteBoard(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	minioClient, err := s.ctx.Minio()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	s.ServiceDelete(config.Bucket, "evalution", data.BoardID)
	s.ServiceDelete(config.Bucket, "teamleader", data.BoardID)
	minioClient.RemoveObject(config.Bucket, "moderator/"+data.BoardID+".png")
	db.Delete(model.Board{}, "id = ?", data.BoardID)
	db.Delete(model.BoardQuestion{}, "board_id = ?", data.BoardID)
	db.Delete(model.BoardAnswer{}, "board_id = ?", data.BoardID)
	db.Delete(model.MemberBoard{}, "board_id = ?", data.BoardID)

	return map[string]interface{}{
		"message": "success",
	}
}

func (s *ModeratorService) ServiceDelete(mybucket, myprefixname, boardid string) {
	minioClient, err := s.ctx.Minio()
	if err != nil {
		log.Println(err)
	}
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	memberboard := []model.MemberBoard{}
	db.Table("member_board").Where("board_id = ?", boardid).Scan(&memberboard)
	for _, v := range memberboard {
		minioClient.RemoveObject(config.Bucket, myprefixname+"/"+boardid+"/"+v.MemberID+".png")
	}
}

func (s *ModeratorService)ExportCsv(data *model.Data) interface{}{
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	defer db.Close()
	result := []model.ResultJoinTeamleader{}
	question := []model.BoardQuestion{}
	member := []model.MemberJoinMemberBoard{}
	var mean, sd, sd2, na float64
	var point, n int
	var csvs [][]string
	csvs = [][]string{{"ค่าเฉลี่ย" , "ส่วนเบี่ยงเบนมาตรฐาน" , "ค่าความแปรปรวน" , "จำนวนครั้งที่ถูกประเมิน"}}
	db.Table("member_board as mb").Select("mb.member_id,m.name").Where("mb.board_id = ? AND mb.status_id = '2'", data.BoardID).Joins("left join member as m on m.id = mb.member_id").Scan(&member)
	db.Table("board_question").Where("board_id = ?", data.BoardID).Scan(&question)
	db.Table("result as r").Select("r.question_section,r.point,t.teamleader_id,r.count,r.member_id").Where("board_id = ?", data.BoardID).Joins("left join teamleader as t on t.member_id = r.member_id").Scan(&result)
	for _, value := range member {
		for _, v := range result {
			if v.TeamleaderID == value.MemberID {
				point += v.Point
				if n < v.Count {
					n = v.Count
				}
			}
		}
		na += float64(n)
		n = 0
	}
	na += float64(n)
	if point != 0 {
		mean = float64(point) / float64(len(result))
		for _, v := range result {
			sd += math.Pow(float64(v.Point)-mean, 2)
		}
		sd = math.Sqrt(sd / float64(len(result)))
		sd2 = math.Pow(sd, 2)
	}
	csvs = append(csvs , []string{fmt.Sprintf("%.2f" , mean) , fmt.Sprintf("%.2f" , sd),fmt.Sprintf("%.2f" , sd2) , fmt.Sprintf("%.2f" , na)})
	file, err := os.Create("./csv/" + data.BoardID + ".csv")
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range csvs {
		log.Println(value)
		err := writer.Write(value)
		if err != nil {
			log.Println(err)
		}
	}
	return map[string]interface{}{
		"message" : "https://"+config.Localhost+"/csv/"+data.BoardID+".csv",
	}
}
