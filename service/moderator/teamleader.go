package moderator

import (
	"fmt"
	"log"
	"math"
	"net/url"
	"project-end-server/config"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service"
	"time"
)

type TeamleaderService struct {
	ctx core.IContext
}

func NewTeamleaderService(ctx core.IContext) service.InterfaceTeamLeader {
	return &TeamleaderService{
		ctx: ctx,
	}
}

func (s *TeamleaderService) GetDataTeamleader(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	var result []model.ResultJoinTeamleader
	var memberboard []model.MemberJoinMemberBoard
	var member []map[string]interface{}
	var p, n, na int
	var sd, mean, sd2 float64
	db.Table("result as r").Select("r.count,r.point,r.member_id,t.teamleader_id").Where("r.board_id = ? AND t.teamleader_id = ?", data.BoardID,data.ID).
		Joins("left join teamleader as t on t.member_id = r.member_id").Scan(&result)
	db.Table("member_board as mb").Select("mb.member_id,m.name,t.teamleader_id").Where("mb.board_id = ? AND t.teamleader_id = ? AND mb.status_id = '3'", data.BoardID,data.ID).
		Joins("left join member as m on m.id = mb.member_id").Joins("left join teamleader as t on t.member_id = mb.member_id").Scan(&memberboard)

	//****************************************************************************************************************//
	for _, v := range memberboard {
		for _, value := range result {
			if v.MemberID == value.MemberID {
				if n < value.Count {
					n = value.Count
				}
			}
		}
		member = append(member, map[string]interface{}{
			"ชื่อ": v.Name,
			"จำนวนครั้งที่ได้ประเมิน":    n,
		})
		na += n
		n = 0
	}
	for _, value := range result {
		p += value.Point
		log.Println(value.Point)
	}
	log.Println(p , len(result))
	if len(result) != 0 {
		n = result[len(result)-1].Count
	}
	if p != 0 {
		mean = float64(p) / float64(len(result))
		for _, v := range result {
			sd += math.Pow(float64(v.Point)-mean, 2)
		}
		sd = math.Sqrt(sd / float64(len(result)))
		sd2 = math.Pow(sd, 2)

	}
	minioClient, err := s.ctx.Minio()
	reqParams := make(url.Values)
	reqParams.Set("response-content-disposition", "attachment; filename=\""+data.ID+".png\"")

	// Generates a presigned url which expires in a day.
	presignedURL, err := minioClient.PresignedGetObject(config.Bucket, "teamleader/"+data.BoardID+"/"+data.ID+".png", time.Second*24*60*60, reqParams)
	if err != nil {
		fmt.Println(err)
	}
	if member == nil {
		member = append(member, map[string]interface{}{
			"ชื่อ": "ไม่มี",
			"จำนวนครั้งที่ได้ประเมิน":    0,
		})
	}

	// Gernerate presigned get object url.
	return map[string]interface{}{
		"evalution": map[string]interface{}{
			"mean": fmt.Sprintf("%.2f", mean),
			"sd":   fmt.Sprintf("%.2f", sd),
			"sd2":  fmt.Sprintf("%.2f", sd2),
		},
		"count": map[string]interface{}{
			"count": na,
		},
		"items":  member,
		"qrcode": presignedURL.String(),
		"link":   "https://" + config.URL + "/checkstatus?boardid=" + data.BoardID + "&status=3&teamleaderid=" + data.ID,
		"status": "2",
	}
}

