package evalution

import (
	"log"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service"
	"time"
)

type EvalutionService struct {
	ctx core.IContext
}

func NewEvalutionService(ctx core.IContext) service.InterfaceEvalution {
	return &EvalutionService{
		ctx: ctx,
	}
}

func (s *EvalutionService) GetFormQrcode(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	if data.TimeID != "" {
		check := model.Checktime{}
		lo, _ := time.LoadLocation("Asia/Bangkok")

		db.Table("checktime").Where("time_id = ? AND board_id = ?", data.TimeID, data.BoardID).Scan(&check)
		times := time.Date(time.Now().In(lo).Year(),time.Now().In(lo).Month(),time.Now().In(lo).Day(),0 ,0,0,0,time.UTC)
		checktime := time.Date(check.CreatedAt.Year(),check.CreatedAt.Month(),check.CreatedAt.Day(),0,0,0,0,time.UTC)
		log.Println(times , checktime)
		if checktime == times {
			return map[string]interface{}{
				"message" : "คุณได้ประเมินไปแล้วในวันนี้",
			}
		}
	}
	board := model.Board{}
	memberboard := model.MemberBoard{}
	member := model.Member{}
	boardcheck := db.Table("board").Where("id = ?", data.BoardID).Scan(&board).NewRecord(board)
	if boardcheck {
		return map[string]interface{}{
			"message": "Not have board in system",
		}
	}
	db.Table("member_board").Where("member_id = ? AND status_id = '2'", data.TeamLeaderID).Scan(&memberboard)
	if memberboard.ID == "" {
		return map[string]interface{}{
			"message": "this teamleader not exist ",
		}
	}
	checkmember := db.Table("member").Where("id = ?", data.ID).Scan(&member).NewRecord(member)
	if checkmember {
		return map[string]interface{}{
			"message": "this member not exist",
		}
	}
	var question []model.BoardJoinBoardQuestion
	var answer []model.BoardAnswer
	var arrAnswer, arrQuestion []map[string]interface{}
	var first = true
	n := 1
	db.Table("board_question as bq").Select("bq.form,b.title,b.des,bq.question").Where("bq.board_id = ?", data.BoardID).Joins("left join board as b on b.id = bq.board_id").Scan(&question)
	db.Table("board_answer").Where("board_id = ?", data.BoardID).Scan(&answer)
	for y, value := range question {
		first = true
		for _, v := range answer {
			if n == 6 {
				n = 1
			}
			if y+1 == v.QuestionSection {
				if value.Form == 0 {
					arrAnswer = append(arrAnswer, map[string]interface{}{
						"answer": v.Answer,
						"val":    n,
					})
				}
				if value.Form == 1 {
					arrAnswer = []map[string]interface{}{
						{
							"maxrate": 5,
						},
					}
				}
				if value.Form == 2 {
					if first {
						arrAnswer = append(arrAnswer, map[string]interface{}{
							"text": "Please select answer",
							"val":  0,
						})
						first = false
					}
					arrAnswer = append(arrAnswer, map[string]interface{}{
						"text": v.Answer,
						"val":  n,
					})
				}
				n++
			}
		}
		if value.Form != 2 {
			arrQuestion = append(arrQuestion, map[string]interface{}{
				"questions": value.Question,
				"options":   arrAnswer,
				"checked":   0,
				"status":    value.Form,
			})
		} else {
			arrQuestion = append(arrQuestion, map[string]interface{}{
				"questions": value.Question,
				"options": map[string]interface{}{
					"list": arrAnswer,
				},
				"checked": "0",
				"status":  value.Form,
			})
		}
		arrAnswer = nil
	}
	return map[string]interface{}{
		"items": arrQuestion,
		"title": question[0].Title,
		"des":   question[0].Des,
	}
}

func (s *EvalutionService) AddPoint(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}

	defer db.Close()
	result := []model.Result{}
	var timeid string
	if data.TimeID == "" {
		timeid, err = model.NewUUID()
		if err != nil {
			log.Println(err)
		}
	} else {
		timeid = data.TimeID
	}
	lo, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		log.Println(err)
	}
	db.Create(&model.Checktime{
		TimeID:    timeid,
		BoardID:   data.BoardID,
		CreatedAt: time.Now().In(lo),
	})
	var lenght int
	db.Table("result").Select("count").Where("board_id = ? AND member_id = ?", data.BoardID, data.ID).Scan(&result)
	if len(result) != 0 {
		lenght = result[len(result)-1].Count + 1
	} else {
		lenght = 1
	}
	for i, v := range data.Point {
		err = db.Create(&model.Result{
			BoardID:         data.BoardID,
			MemberID:        data.ID,
			QuestionSection: i + 1,
			Point:           v.Checked,
			Count:           lenght,
		}).Error
		if err != nil {
			return map[string]interface{}{
				"message": "add point fail",
			}
		}
	}
	return map[string]interface{}{
		"message": "success",
		"timeid":  timeid,
	}
}
