package service

import "project-end-server/model"

type InterfaceAdmin interface {
	AddModerator(data *model.Data) interface{}
	LoginAdmin(data *model.Data) interface{}
	GetModerator(data *model.Data) interface{}
	DeleteModerator(data *model.Data) interface{}
	GetMember(data *model.Data) interface{}
	DeleteMember(data *model.Data) interface{}
}

type InterfaceModerator interface {
	GetBoard(data *model.Data) interface{}
	CreateBoard(data *model.Data) interface{}
	DeleteBoard(data *model.Data) interface{}
	SetEvalution(data *model.Data) interface{}
	GetEvalution(data *model.Data) interface{}
	GetDataModerator(data *model.Data) interface{}
	ExportCsv(data *model.Data) interface{}
}

type InterfaceLogin interface {
	Login(data *model.Data) interface{}
	Logout(data *model.Data) interface{}
}
type InterfaceRegister interface {
	Register(data *model.Data) interface{}
	GoogleSignUp(data *model.Data, tokens string) interface{}
	//SignInGoogle
}

type InterfaceEvalution interface {
	GetFormQrcode(data *model.Data) interface{}
	AddPoint(data *model.Data) interface{}
}

type InterfaceTeamLeader interface {
	GetDataTeamleader(data *model.Data) interface{}
}

type InterfaceMember interface {
	GetDataMember(data *model.Data) interface{}
	LeaveBoard(data *model.Data) interface{}
	Inboard(data *model.Data) interface{}
}
