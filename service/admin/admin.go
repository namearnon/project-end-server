package admin

import (
	"log"
	"project-end-server/config"
	"project-end-server/core"
	"project-end-server/model"
	"project-end-server/service"
)

type ServiceAdmin struct {
	ctx core.IContext
}

func NewServiceAdmin(ctx core.IContext) service.InterfaceAdmin {
	return &ServiceAdmin{
		ctx: ctx,
	}
}

func (s *ServiceAdmin) LoginAdmin(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	a := &model.Login{}

	password := model.Admin{}
	checkuser := db.Table("admin").Where("username = ?", data.Username).Scan(&password).NewRecord(password)
	if checkuser {
		return map[string]interface{}{
			"message": "Not have username in system",
		}
	} else {
		if password.StatusID != 0 {
			return map[string]interface{}{
				"message": "Username is not admin",
			}
		}
	}
	if password.Password != data.Password {
		return map[string]interface{}{
			"message": "Password not match",
		}
	}
	checktoken := db.Table("login").Where("id = ?", data.Username).Scan(&a).NewRecord(a)
	if !checktoken {
		db.Delete(&model.Login{}, "id = ?", data.Username)
	}
	token := model.GenerateToken(150)
	db.Create(&model.Login{
		ID:    data.Username,
		Token: token,
	})
	return map[string]interface{}{
		"id":     data.Username,
		"token":  token,
		"status": password.StatusID,
	}
}

func (s *ServiceAdmin) AddModerator(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	id, err := model.NewUUID()
	check := model.Admin{}
	db.Table("admin").Where("username = ?", data.Username).Scan(&check)
	if check.Username == data.Username {
		return map[string]interface{}{
			"message": "Username has exist",
		}
	}
	db.Create(&model.Admin{
		ID:       id,
		Username: data.Username,
		Name:     data.Name,
		Password: model.HashPassword([]byte(data.Password)),
		StatusID: 1,
	})
	return map[string]interface{}{
		"message": "success",
	}
}

func (s *ServiceAdmin) GetModerator(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	moderator := []model.Admin{}
	alldata := []map[string]interface{}{}
	db.Table("admin").Where("status_id = '1'").Scan(&moderator)
	for _, v := range moderator {
		alldata = append(alldata, map[string]interface{}{
			"id":       v.ID,
			"username": v.Username,
			"name":     v.Name,
		})
	}
	return map[string]interface{}{
		"message": alldata,
	}
}

func (s *ServiceAdmin) DeleteModerator(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	minioClient, err := s.ctx.Minio()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	var board []model.Board
	db.Delete(model.Admin{}, "id = ?", data.ModeratorID)
	db.Table("board").Where("create_by = ?", data.ModeratorID).Scan(&board)
	for _, v := range board {
		s.ServiceDelete(config.Bucket, "evalution"  ,v.CreateBy)
		s.ServiceDelete(config.Bucket, "teamleader" ,v.CreateBy)
		minioClient.RemoveObject(config.Bucket, "moderator/"+v.CreateBy+".png")
		db.Delete(model.MemberBoard{}, "board_id = ?", v.CreateBy)
		db.Delete(model.BoardQuestion{}, "board_id = ?", v.CreateBy)
		db.Delete(model.BoardAnswer{}, "board_id = ?", v.CreateBy)
		db.Delete(model.Result{}, "board_id = ?", v.CreateBy)
	}
	db.Delete(model.Board{}, "create_by = ?", data.ModeratorID)

	return map[string]interface{}{
		"message": "success",
	}
}

func (s *ServiceAdmin) ServiceDelete(mybucket, myprefixname , boardid string) {
	minioClient, err := s.ctx.Minio()
	if err != nil {
		log.Println(err)
	}
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	memberboard := []model.MemberBoard{}
	db.Table("member_board").Where("board_id = ?",boardid).Scan(&memberboard)
	for _ , v := range memberboard {
		minioClient.RemoveObject(config.Bucket, myprefixname + "/"+boardid+"/"+v.MemberID+".png")
	}
}



func (s *ServiceAdmin) GetMember(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	var member []model.Member
	var allmember []map[string]interface{}
	db.Table("member").Scan(&member)
	for _, v := range member {
		allmember = append(allmember, map[string]interface{}{
			"id":       v.ID,
			"username": v.Email,
			"name":     v.Password,
		})
	}
	return map[string]interface{}{
		"message": allmember,
	}
}

func (s *ServiceAdmin) DeleteMember(data *model.Data) interface{} {
	db, err := s.ctx.DB()
	if err != nil {
		log.Println(err)
	}
	defer db.Close()
	db.Delete(model.Member{}, "id = ?", data.ID)
	db.Delete(model.MemberBoard{}, "member_id = ? OR teamleader_id = ?", data.ID, data.ID)
	db.Delete(model.Result{}, "member_id = ? OR teamleader_id = ?", data.ID, data.ID)
	return map[string]interface{}{
		"message": "success",
	}
}
