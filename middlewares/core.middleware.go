package middlewares

import (
	"fmt"
	"os"
	"project-end-server/core"
"github.com/labstack/echo/v4"
)

func Core(h echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		cc := &core.Context{c}
		return h(cc)
	}
}

func GetPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = "9999"
		fmt.Println("No Port In Heroku" + port)
	}
	return ":" + port
}